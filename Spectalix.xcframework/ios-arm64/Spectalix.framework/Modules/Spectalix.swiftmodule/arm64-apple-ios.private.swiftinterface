// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 6.0 effective-5.10 (swiftlang-6.0.0.9.10 clang-1600.0.26.2)
// swift-module-flags: -target arm64-apple-ios13.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -enable-bare-slash-regex -module-name Spectalix
// swift-module-flags-ignorable: -no-verify-emitted-module-interface
import AVFoundation
import AVKit
import Accelerate
import CommonCrypto
import CoreGraphics
import CoreImage
import CoreML
import CoreMedia
import CoreMotion
import CoreServices
import CryptoKit
import DeveloperToolsSupport
import Foundation
import GLKit
import Metal
import MetalKit
import Photos
import Security
import Swift
import SwiftUI
import SystemConfiguration
import UIKit
import WebKit
import _Concurrency
import _StringProcessing
import _SwiftConcurrencyShims
import simd
extension Spectalix.ReorderController : UIKit.UIGestureRecognizerDelegate {
  @_Concurrency.MainActor @preconcurrency @objc dynamic public func gestureRecognizerShouldBegin(_ gestureRecognizer: UIKit.UIGestureRecognizer) -> Swift.Bool
}
extension UIKit.UILayoutPriority {
  public static let sxMiddle: UIKit.UILayoutPriority
  public static let sxAlmostRequired: UIKit.UILayoutPriority
}
@_cdecl("sxChangeInterfaceOrientationAccordingly")
public func sxChangeInterfaceOrientationAccordingly(orientationMask: UIKit.UIInterfaceOrientationMask)
@_cdecl("sxSupportedInterfaceOrientations")
public func sxSupportedInterfaceOrientations(hostAppInterfaceOrientationMask: UIKit.UIInterfaceOrientationMask) -> UIKit.UIInterfaceOrientationMask
@_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers @objc @_Concurrency.MainActor @preconcurrency final public class SXBaseViewController : UIKit.UIViewController {
  @_Concurrency.MainActor @preconcurrency @objc override final public var preferredStatusBarStyle: UIKit.UIStatusBarStyle {
    @objc get
  }
  @objc @_Concurrency.MainActor @preconcurrency final public var onBackFromSpectalixMainScreen: (() -> Swift.Void)?
  @objc @_Concurrency.MainActor @preconcurrency final public var onRequestFullScreenMode: ((Swift.Bool) -> Swift.Void)?
  @_Concurrency.MainActor @preconcurrency @objc dynamic public init()
  @objc deinit
  @objc @_Concurrency.MainActor @preconcurrency final public func setupUIColors(_ backgroundColor: UIKit.UIColor, statusBarTextColor: UIKit.UIColor, noInternetMessageLabelTextColor: UIKit.UIColor)
  @objc @_Concurrency.MainActor @preconcurrency final public var haveFocus: Swift.Bool {
    get
  }
  @objc @_Concurrency.MainActor @preconcurrency final public func processGotFocus()
  @objc @_Concurrency.MainActor @preconcurrency final public func processLostFocus()
  @_Concurrency.MainActor @preconcurrency @objc override final public func viewDidLoad()
  @_Concurrency.MainActor @preconcurrency @objc override final public func viewWillAppear(_ animated: Swift.Bool)
  @_Concurrency.MainActor @preconcurrency final public func trySwitchToActualScreen() -> Swift.Bool
}
public enum SXPhoneNumberFormat {
  case E164, International, National, RFC3966
  public static func == (a: Spectalix.SXPhoneNumberFormat, b: Spectalix.SXPhoneNumberFormat) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public enum SXPhoneNumberCountryCode : Swift.String {
  case AF
  case ZA
  case AL
  case DZ
  case DE
  case AD
  case AO
  case AI
  case AG
  case SA
  case AR
  case AM
  case AW
  case AU
  case AT
  case AZ
  case BS
  case BH
  case BD
  case BB
  case BE
  case BZ
  case BM
  case BT
  case BY
  case BO
  case BA
  case BW
  case BN
  case BR
  case BG
  case BF
  case BI
  case BJ
  case KH
  case CM
  case CA
  case CV
  case CL
  case CN
  case CY
  case CO
  case KM
  case CG
  case CD
  case KP
  case KR
  case CR
  case HR
  case CU
  case CI
  case DK
  case DJ
  case DM
  case ES
  case EE
  case FJ
  case FI
  case FR
  case GA
  case GM
  case GH
  case GI
  case GD
  case GL
  case GR
  case GP
  case GU
  case GT
  case GG
  case GN
  case GQ
  case GW
  case GY
  case GF
  case GE
  case HT
  case HN
  case HK
  case HU
  case IN
  case ID
  case IQ
  case IR
  case IE
  case IS
  case IL
  case IT
  case JM
  case JP
  case JE
  case JO
  case KZ
  case KE
  case KG
  case KI
  case KW
  case RE
  case LA
  case LS
  case LV
  case LB
  case LY
  case LR
  case LI
  case LT
  case LU
  case MO
  case MK
  case MG
  case MY
  case MW
  case MV
  case ML
  case MT
  case MA
  case MQ
  case MU
  case MR
  case YT
  case MX
  case MD
  case MC
  case MN
  case MS
  case ME
  case MZ
  case MM
  case NA
  case NR
  case NI
  case NE
  case NG
  case NU
  case NO
  case NC
  case NZ
  case NP
  case OM
  case UG
  case UZ
  case PK
  case PW
  case PA
  case PG
  case PY
  case NL
  case PH
  case PL
  case PF
  case PR
  case PT
  case PE
  case QA
  case RO
  case GB
  case RU
  case RW
  case CF
  case DO
  case BL
  case KN
  case SM
  case MF
  case PM
  case VC
  case SH
  case LC
  case SV
  case WS
  case AS
  case ST
  case RS
  case SC
  case SL
  case SG
  case SK
  case SI
  case SO
  case SD
  case SS
  case LK
  case CH
  case SR
  case SE
  case SJ
  case SZ
  case SY
  case SN
  case TJ
  case TZ
  case TW
  case TD
  case CZ
  case IO
  case PS
  case TH
  case TL
  case TG
  case TK
  case TO
  case TT
  case TN
  case TM
  case TR
  case TV
  case UA
  case UY
  case VU
  case VE
  case VN
  case WF
  case YE
  case ZM
  case ZW
  case EG
  case AE
  case EC
  case ER
  case VA
  case FM
  case US
  case ET
  case CX
  case NF
  case IM
  case KY
  case CC
  case CK
  case FO
  case FK
  case MP
  case MH
  case SB
  case TC
  case VG
  case VI
  case AX
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
public let USE_RENDER_METAL: Swift.Bool
extension UIKit.UIColor {
  public var hsbaComponents: (hue: CoreFoundation.CGFloat, saturation: CoreFoundation.CGFloat, brightness: CoreFoundation.CGFloat, alpha: CoreFoundation.CGFloat) {
    get
  }
}
@_cdecl("sxInitialize")
public func sxInitialize(tenantId: Swift.String)
@_cdecl("sxApplicationWillTerminate")
public func sxApplicationWillTerminate()
extension UIKit.UITableView {
  @_Concurrency.MainActor @preconcurrency public var reorder: Spectalix.ReorderController {
    get
  }
}
public enum ReorderSpacerCellStyle {
  case automatic
  case hidden
  case transparent
  public static func == (a: Spectalix.ReorderSpacerCellStyle, b: Spectalix.ReorderSpacerCellStyle) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public protocol TableViewReorderDelegate : AnyObject {
  func tableView(_ tableView: UIKit.UITableView, reorderRowAt sourceIndexPath: Foundation.IndexPath, to destinationIndexPath: Foundation.IndexPath)
  func tableView(_ tableView: UIKit.UITableView, canReorderRowAt indexPath: Foundation.IndexPath) -> Swift.Bool
  func tableView(_ tableView: UIKit.UITableView, targetIndexPathForReorderFromRowAt sourceIndexPath: Foundation.IndexPath, to proposedDestinationIndexPath: Foundation.IndexPath) -> Foundation.IndexPath
  func tableViewDidBeginReordering(_ tableView: UIKit.UITableView, at indexPath: Foundation.IndexPath)
  func tableViewDidFinishReordering(_ tableView: UIKit.UITableView, from initialSourceIndexPath: Foundation.IndexPath, to finalDestinationIndexPath: Foundation.IndexPath)
}
extension Spectalix.TableViewReorderDelegate {
  public func tableView(_ tableView: UIKit.UITableView, canReorderRowAt indexPath: Foundation.IndexPath) -> Swift.Bool
  public func tableView(_ tableView: UIKit.UITableView, targetIndexPathForReorderFromRowAt sourceIndexPath: Foundation.IndexPath, to proposedDestinationIndexPath: Foundation.IndexPath) -> Foundation.IndexPath
  public func tableViewDidBeginReordering(_ tableView: UIKit.UITableView, at indexPath: Foundation.IndexPath)
  public func tableViewDidFinishReordering(_ tableView: UIKit.UITableView, from initialSourceIndexPath: Foundation.IndexPath, to finalDestinationIndexPath: Foundation.IndexPath)
}
@objc @_hasMissingDesignatedInitializers public class ReorderController : ObjectiveC.NSObject {
  weak public var delegate: (any Spectalix.TableViewReorderDelegate)?
  public var isEnabled: Swift.Bool {
    get
    set
  }
  public var longPressDuration: Swift.Double {
    get
    set
  }
  public var cancelsTouchesInView: Swift.Bool {
    get
    set
  }
  public var animationDuration: Swift.Double
  public var cellOpacity: CoreFoundation.CGFloat
  public var cellScale: CoreFoundation.CGFloat
  public var shadowColor: UIKit.UIColor
  public var shadowOpacity: CoreFoundation.CGFloat
  public var shadowRadius: CoreFoundation.CGFloat
  public var shadowOffset: CoreFoundation.CGSize
  public var spacerCellStyle: Spectalix.ReorderSpacerCellStyle
  public var autoScrollEnabled: Swift.Bool
  public func spacerCell(for indexPath: Foundation.IndexPath) -> UIKit.UITableViewCell?
  @objc deinit
}
extension Spectalix.SXPhoneNumberFormat : Swift.Equatable {}
extension Spectalix.SXPhoneNumberFormat : Swift.Hashable {}
extension Spectalix.SXPhoneNumberCountryCode : Swift.Equatable {}
extension Spectalix.SXPhoneNumberCountryCode : Swift.Hashable {}
extension Spectalix.SXPhoneNumberCountryCode : Swift.RawRepresentable {}
extension Spectalix.ReorderSpacerCellStyle : Swift.Equatable {}
extension Spectalix.ReorderSpacerCellStyle : Swift.Hashable {}
